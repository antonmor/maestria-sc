SET FOREIGN_KEY_CHECKS=0;

TRUNCATE anexopdf;
TRUNCATE expediente; 
TRUNCATE enviasa;
TRUNCATE secretariaacuerdos;
TRUNCATE seguimiento;
TRUNCATE admision;
TRUNCATE desechado;
TRUNCATE notificacion;
TRUNCATE involucrados;



SET FOREIGN_KEY_CHECKS=1;

ALTER TABLE anexopdf AUTO_INCREMENT = 1;
ALTER TABLE  expediente AUTO_INCREMENT = 1; 
ALTER TABLE  enviasa AUTO_INCREMENT = 1;
ALTER TABLE  secretariaacuerdos AUTO_INCREMENT = 1;
ALTER TABLE seguimiento AUTO_INCREMENT = 1;
ALTER TABLE admision AUTO_INCREMENT = 1;
ALTER TABLE desechado AUTO_INCREMENT = 1;
ALTER table notificacion AUTO_INCREMENT = 1;
ALTER table involucrados AUTO_INCREMENT = 1;