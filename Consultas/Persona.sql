DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `PersonaDom`(
in tipo varchar(50),
in nombre varchar(45),
in apat varchar(45),
in amat varchar(45),
in rsocial varchar(45),
in genero varchar(45),
in identificacion varchar(45),
in referencia varchar(45),
in tf varchar(45),
in movil varchar(45),
in email varchar(45),
in cemail varchar(45),
in Estado varchar(45),
in Municipio varchar(45),
in Dom varchar(45),
in interior varchar(45),
in exterior varchar(45),
in cp varchar(45),
in Estado1 varchar(45),
in Municipio1 varchar(45),
in Dom1 varchar(45),
in interior1 varchar(45),
in exterior1 varchar(45),
in cp1 varchar(45),
in refe varchar(45),
in user varchar(45),
in pass varchar(45),
in curp varchar(45)

)
BEGIN
    DECLARE _id_persona INT DEFAULT 0;
    DECLARE _id_domicilio INT DEFAULT 0;
   

insert into Persona
	  (`RazonSocial`,`Nombre`,`Apat`,`Amat`,`Tel`,`Cel`,`Email`,`CURP`,`Tipo de Persona`,`IDoficial`,`NumeroIDOficial`,`Usuario`,`Password`)
values(rsocial,nombre,apat,amat,tf,movil,email,curp,tipo,identificacion,referencia,user,AES_ENCRYPT(pass,'sistema2016'));

SET _id_persona = last_insert_id();    

insert into Domicilio(`id_Persona`,	`Domicilio`,	`Municipio`,	`Estado`,	`Cp`,	`Referencia`,	`Oir`,	`NInterior`,	`NExterior`,	`status`) 
values(_id_persona,	Dom,Municipio,Estado,cp,'NULL','0',interior,exterior,'1');  

insert into Domicilio(`id_Persona`,	`Domicilio`,	`Municipio`,	`Estado`,	`Cp`,	`Referencia`,	`Oir`,	`NInterior`,	`NExterior`,	`status`) 
values(_id_persona,	Dom1,Municipio1,Estado1,cp1,refe,'1',interior1,exterior1,'1');

insert into RolesPeriodo(`idPersona`,	`idRoles`,	`Fecha`,	`Status`)
values(_id_persona,'7',now(),'1');

END$$
DELIMITER ;
