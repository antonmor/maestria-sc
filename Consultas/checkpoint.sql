SELECT s.fechasis as 'Fecha de actividad',Expediente, mov as 'Movimiento',TS.Tipo as 'Concepto', act.Tipo as 'Documento' , m.modulo as 'Se encuentra en el módulo', concat(p.nombre,' ',p.Apat,' ',p.Amat) as 'Responsable',
s.Comentarios
FROM TCAbd.Seguimiento s 
join Modulo m on m.idmodulo = s.idmodulo 
join persona p on p.id = s.id_op
join Expediente e on e.id = s.idExpediente
join TipoSeguimiento TS on TS.Id_Ts = s.id_Tseguimiento
left join AnexoPDF ap on ap.id = s.AnexoPDF_id
left join AcuerdoTipo act on act.id = ap.id_Tipo
order by  s.fechasis
